# Terraform source code example to deploy an ephemeral AWS EKS Cluster

This example is used in this blog post: [https://dev.to/katiatalh/provision-ephemeral-kubernetes-clusters-on-aws-eks-using-terraform-and-gitlab-ci-cd-3f74/edit](https://dev.to/katiatalh/provision-ephemeral-kubernetes-clusters-on-aws-eks-using-terraform-and-gitlab-ci-cd-3f74/edit)
that shows how to provision ephemeral Kubernetes clusters in AWS EKS using Terraform and Gitlab CI/CD.
