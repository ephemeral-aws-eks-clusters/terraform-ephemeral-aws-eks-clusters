variable "cluster_prefix_name" {}

variable "region" {
  default = "eu-west-1"
}

variable "aws_elb_hostname" {
  default = ""
}

variable "route53_record_name" {
  default = ""
}

variable "aws_zone_id" {
  default = ""
}
